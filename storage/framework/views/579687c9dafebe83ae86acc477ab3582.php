/**
     * @OA\Get(
     *      path="/<?php echo e($config->modelNames->dashedPlural); ?>",
     *      summary="get<?php echo e($config->modelNames->name); ?>List",
     *      tags={"<?php echo e($config->modelNames->name); ?>"},
     *      description="Get all <?php echo e($config->modelNames->plural); ?>",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/<?php echo e($config->modelNames->name); ?>")
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */<?php /**PATH /var/www/bcsrp-v2/resources/views/vendor/swagger-generator/controller/index.blade.php ENDPATH**/ ?>