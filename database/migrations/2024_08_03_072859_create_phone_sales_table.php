<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('phone_sales', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('company_number');
            $table->string('company_type');
            $table->string('company_description');
            $table->string('company_manager_name');
            $table->string('company_address');
            $table->string('company_reaction');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('phone_sales');
    }
};
