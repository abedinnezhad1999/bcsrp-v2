<?php

namespace App\Http\Controllers;

use App\Models\CustomField;
use App\Models\PhoneSale;
use Illuminate\Http\Request;

class PhoneSaleController extends Controller
{
    public function index()
    {
        if(\Auth::user()->can('create bank account'))
        {
            $phoneSales = PhoneSale::where('created_by', '=', \Auth::user()->creatorId())->get();

            return view('phoneSale.index', compact('phoneSales'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function create()
    {
        if(\Auth::user()->can('create bank account'))
        {
            $customFields = CustomField::where('created_by', '=', \Auth::user()->creatorId())->where('module', '=', 'account')->get();

            return view('phoneSale.create', compact('customFields'));
        }
        else
        {
            return response()->json(['error' => __('Permission denied.')], 401);
        }
    }

    public function store(Request $request)
    {
        if(\Auth::user()->can('create bank account'))
        {

            $phoneSale                        = new PhoneSale();
            $phoneSale->id                    = $request->id;
            $phoneSale->company_name          = $request->company_name;
            $phoneSale->company_number        = $request->company_number;
            $phoneSale->company_type          = $request->company_type;
            $phoneSale->company_description   = $request->company_description;
            $phoneSale->company_manager_name  = $request->company_manager_name;
            $phoneSale->company_address       = $request->company_address;
            $phoneSale->company_reaction      = $request->company_reaction;
            $phoneSale->created_by            = \Auth::user()->creatorId();
            $phoneSale->save();
            CustomField::saveData($phoneSale, $request->customField);

            return redirect()->route('phone-sale.index')->with('success', __('Account successfully created.'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function show()
    {
        return redirect()->route('phone-sale.index');
    }


    public function edit(PhoneSale $phoneSale)
    {
        if(\Auth::user()->can('edit bank account'))
        {
            if($phoneSale->created_by == \Auth::user()->creatorId())
            {
                $customFields             = CustomField::where('created_by', '=', \Auth::user()->creatorId())->where('module', '=', 'account')->get();

                return view('phoneSale.edit', compact('customFields','phoneSale'));
            }
            else
            {
                return response()->json(['error' => __('Permission denied.')], 401);
            }
        }
        else
        {
            return response()->json(['error' => __('Permission denied.')], 401);
        }
    }


    public function update(Request $request, PhoneSale $phoneSale)
    {
        if(\Auth::user()->can('create bank account'))
        {


            $phoneSale->company_name          = $request->company_name;
            $phoneSale->company_number        = $request->company_number;
            $phoneSale->company_type          = $request->company_type;
            $phoneSale->company_description   = $request->company_description;
            $phoneSale->company_manager_name  = $request->company_manager_name;
            $phoneSale->company_address       = $request->company_address;
            $phoneSale->company_reaction      = $request->company_reaction;
            $phoneSale->created_by       = \Auth::user()->creatorId();
            $phoneSale->save();
            CustomField::saveData($phoneSale, $request->customField);

            return redirect()->route('phone-sale.index')->with('success', __('Account successfully updated.'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }


    public function destroy(PhoneSale $phoneSale)
    {
        if(\Auth::user()->can('delete bank account'))
        {
            if($phoneSale->created_by == \Auth::user()->creatorId())
            {

                    $phoneSale->delete();

                    return redirect()->route('phone-sale.index')->with('success', __('Account successfully deleted.'));


            }
            else
            {
                return redirect()->back()->with('error', __('Permission denied.'));
            }
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }
}
