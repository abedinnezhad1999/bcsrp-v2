<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAmirRequest;
use App\Http\Requests\UpdateAmirRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Amir;
use Illuminate\Http\Request;
use Flash;

class AmirController extends AppBaseController
{
    /**
     * Display a listing of the Amir.
     */
    public function index(Request $request)
    {
        /** @var Amir $amirs */
        $amirs = Amir::paginate(10);

        return view('amirs.index')
            ->with('amirs', $amirs);
    }


    /**
     * Show the form for creating a new Amir.
     */
    public function create()
    {
        return view('amirs.create');
    }

    /**
     * Store a newly created Amir in storage.
     */
    public function store(CreateAmirRequest $request)
    {
        $input = $request->all();

        /** @var Amir $amir */
        $amir = Amir::create($input);

        Flash::success('Amir saved successfully.');

        return redirect(route('amirs.index'));
    }

    /**
     * Display the specified Amir.
     */
    public function show($id)
    {
        /** @var Amir $amir */
        $amir = Amir::find($id);

        if (empty($amir)) {
            Flash::error('Amir not found');

            return redirect(route('amirs.index'));
        }

        return view('amirs.show')->with('amir', $amir);
    }

    /**
     * Show the form for editing the specified Amir.
     */
    public function edit($id)
    {
        /** @var Amir $amir */
        $amir = Amir::find($id);

        if (empty($amir)) {
            Flash::error('Amir not found');

            return redirect(route('amirs.index'));
        }

        return view('amirs.edit')->with('amir', $amir);
    }

    /**
     * Update the specified Amir in storage.
     */
    public function update($id, UpdateAmirRequest $request)
    {
        /** @var Amir $amir */
        $amir = Amir::find($id);

        if (empty($amir)) {
            Flash::error('Amir not found');

            return redirect(route('amirs.index'));
        }

        $amir->fill($request->all());
        $amir->save();

        Flash::success('Amir updated successfully.');

        return redirect(route('amirs.index'));
    }

    /**
     * Remove the specified Amir from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Amir $amir */
        $amir = Amir::find($id);

        if (empty($amir)) {
            Flash::error('Amir not found');

            return redirect(route('amirs.index'));
        }

        $amir->delete();

        Flash::success('Amir deleted successfully.');

        return redirect(route('amirs.index'));
    }
}
