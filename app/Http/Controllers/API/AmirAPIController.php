<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAmirAPIRequest;
use App\Http\Requests\API\UpdateAmirAPIRequest;
use App\Models\Amir;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AmirResource;

/**
 * Class AmirController
 */

class AmirAPIController extends AppBaseController
{
    /**
     * @OA\Get(
     *      path="/amirs",
     *      summary="getAmirList",
     *      tags={"Amir"},
     *      description="Get all Amirs",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Amir")
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request): JsonResponse
    {
        $query = Amir::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $amirs = $query->get();

        return $this->sendResponse(AmirResource::collection($amirs), 'Amirs retrieved successfully');
    }

    /**
     * @OA\Post(
     *      path="/amirs",
     *      summary="createAmir",
     *      tags={"Amir"},
     *      description="Create Amir",
     *      @OA\RequestBody(
     *        required=true,
     *        @OA\JsonContent(ref="#/components/schemas/Amir")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Amir"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAmirAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Amir $amir */
        $amir = Amir::create($input);

        return $this->sendResponse(new AmirResource($amir), 'Amir saved successfully');
    }

    /**
     * @OA\Get(
     *      path="/amirs/{id}",
     *      summary="getAmirItem",
     *      tags={"Amir"},
     *      description="Get Amir",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Amir",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Amir"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id): JsonResponse
    {
        /** @var Amir $amir */
        $amir = Amir::find($id);

        if (empty($amir)) {
            return $this->sendError('Amir not found');
        }

        return $this->sendResponse(new AmirResource($amir), 'Amir retrieved successfully');
    }

    /**
     * @OA\Put(
     *      path="/amirs/{id}",
     *      summary="updateAmir",
     *      tags={"Amir"},
     *      description="Update Amir",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Amir",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *        required=true,
     *        @OA\JsonContent(ref="#/components/schemas/Amir")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Amir"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAmirAPIRequest $request): JsonResponse
    {
        /** @var Amir $amir */
        $amir = Amir::find($id);

        if (empty($amir)) {
            return $this->sendError('Amir not found');
        }

        $amir->fill($request->all());
        $amir->save();

        return $this->sendResponse(new AmirResource($amir), 'Amir updated successfully');
    }

    /**
     * @OA\Delete(
     *      path="/amirs/{id}",
     *      summary="deleteAmir",
     *      tags={"Amir"},
     *      description="Delete Amir",
     *      @OA\Parameter(
     *          name="id",
     *          description="id of Amir",
     *           @OA\Schema(
     *             type="integer"
     *          ),
     *          required=true,
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id): JsonResponse
    {
        /** @var Amir $amir */
        $amir = Amir::find($id);

        if (empty($amir)) {
            return $this->sendError('Amir not found');
        }

        $amir->delete();

        return $this->sendSuccess('Amir deleted successfully');
    }
}
