<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhoneSale extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_name',
        'company_number',
        'company_type',
        'company_description',
        'company_manager_name',
        'company_address',
        'company_reaction',
        'created_by',
    ];



}
