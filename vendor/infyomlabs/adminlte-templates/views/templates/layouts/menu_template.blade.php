
<li
    class="dash-item dash-hasmenu {{ Request::segment(1) == 'email_template' || Request::route()->getName() == $config->prefixes->getRoutePrefixWith('.') && $config->modelNames->camelPlural ? ' active dash-trigger' : 'collapsed' }}">
    <a href="@{{ route('{!! $config->prefixes->getRoutePrefixWith('.') !!}{!! $config->modelNames->camelPlural !!}.index') }}"
       class="dash-link">
        <span class="dash-micon"><i class="ti ti-template"></i></span>
        @if($config->options->localized)
            <span class="dash-mtext">@@lang('models/{{ $config->modelNames->camelPlural }}.plural')</span>
        @else
            <span class="dash-mtext">{{ $config->modelNames->humanPlural }}</span>
        @endif
    </a>
</li>
