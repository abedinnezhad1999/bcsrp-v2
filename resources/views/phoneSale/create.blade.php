{{ Form::open(array('url' => 'phone-sale')) }}
<div class="modal-body">
    <div class="row">
        <div class="form-group col-md-6">
            {{ Form::label('company_name', __('نام شرکت'),['class'=>'form-label']) }}
            {{ Form::text('company_name', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('company_number', __('شماره های تماس شرکت'),['class'=>'form-label']) }}
            {{ Form::text('company_number', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('company_type', __('نوع شرکت'),['class'=>'form-label']) }}
            {{ Form::text('company_type', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('company_description', __('توضیحاتی درباره شرکت'),['class'=>'form-label']) }}
            {{ Form::textarea('company_description', '', array('class' => 'form-control',)) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('company_manager_name', __('نام مسئولین شرکت'),['class'=>'form-label']) }}
            {{ Form::text('company_manager_name', '', array('class' => 'form-control',)) }}
        </div>
        <div class="form-group col-md-6">
            {{ Form::label('company_address', __('آدرس شرکت'),['class'=>'form-label']) }}
            {{ Form::textarea('company_address', '', array('class' => 'form-control',)) }}

        </div>
        <div class="form-group col-md-12">
            {{ Form::label('company_reaction', __('جوابی که دادند'),['class'=>'form-label']) }}
            {{ Form::text('company_reaction', '', array('class' => 'form-control',)) }}
        </div>
        @if(!$customFields->isEmpty())
            <div class="col-md-12">
                <div class="tab-pane fade show" id="tab-2" role="tabpanel">
                    @include('customFields.formBuilder')
                </div>
            </div>
        @endif

    </div>
</div>
<div class="modal-footer">
    <input type="button" value="{{__('Cancel')}}" class="btn  btn-light" data-bs-dismiss="modal">
    <input type="submit" value="{{__('Create')}}" class="btn  btn-primary">
</div>
{{ Form::close() }}
