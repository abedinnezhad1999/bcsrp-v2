@extends('layouts.admin')
@section('page-title')
    {{__('Manage Bank Account')}}
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
    <li class="breadcrumb-item">{{__('Bank Account')}}</li>
@endsection

@section('action-btn')
    <div class="float-end">
        @can('create bank account')
            <a href="#" data-url="{{ route('phone-sale.create') }}" data-ajax-popup="true" data-size="lg" data-bs-toggle="tooltip" title="{{__('Create')}}" data-title="{{__('Create New Bank Account')}}" class="btn btn-sm btn-primary">
                <i class="ti ti-plus"></i>
            </a>

        @endcan
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body table-border-style table-border-style">
                    <h5></h5>
                    <div class="table-responsive">
                        <table class="table datatable">
                            <thead>
                            <tr>
                                <th>{{__('نام شرکت')}}</th>
                                <th>{{__('شماره های تماس شرکت')}}</th>
                                <th>{{__('نوع شرکت')}}</th>
                                <th>{{__('توضیحاتی درباره شرکت')}}</th>
                                <th>{{__('نام مسئولین شرکت')}}</th>
                                <th>{{__('آدرس شرکت')}}</th>
                                <th>{{__('پاسخی که دادند')}}</th>
                                <th width="10%"> {{__('Action')}}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($phoneSales as $phoneSale)
                                <tr class="font-style">
                                    <td>{{  $phoneSale->company_name}}</td>
                                    <td>{{  $phoneSale->company_number}}</td>
                                    <td>{{  $phoneSale->company_type}}</td>
                                    <td>{{  $phoneSale->company_description}}</td>
                                    <td>{{  $phoneSale->company_manager_name}}</td>
                                    <td>{{  $phoneSale->company_address}}</td>
                                    <td>{{  $phoneSale->company_reaction}}</td>
                                    @if(Gate::check('edit bank account') || Gate::check('delete bank account'))
                                        <td class="Action">
                                            <span>
                                                    @can('edit bank account')
                                                        <div class="action-btn bg-primary ms-2">
                                                            <a href="#" class="mx-3 btn btn-sm align-items-center" data-url="{{ route('phone-sale.edit',$phoneSale->id) }}" data-ajax-popup="true" title="{{__('Edit')}}" data-title="{{__('Edit Bank Account')}}"data-bs-toggle="tooltip"  data-size="lg"  data-original-title="{{__('Edit')}}">
                                                                <i class="ti ti-pencil text-white"></i>
                                                            </a>
                                                        </div>
                                                    @endcan
                                                    @can('delete bank account')
                                                            <div class="action-btn bg-danger ms-2">
                                                                {!! Form::open(['method' => 'DELETE', 'route' => ['phone-sale.destroy', $phoneSale->id],'id'=>'delete-form-'.$phoneSale->id]) !!}
                                                                <a href="#" class="mx-3 btn btn-sm align-items-center bs-pass-para" data-bs-toggle="tooltip" title="{{__('Delete')}}" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$phoneSale->id}}').submit();">
                                                                    <i class="ti ti-trash text-white text-white"></i>
                                                                </a>
                                                                {!! Form::close() !!}
                                                            </div>
                                                    @endcan

                                            </span>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
