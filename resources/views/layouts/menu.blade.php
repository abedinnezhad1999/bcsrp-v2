

<li class="nav-item">
    <a href="{{ route('abedins.index') }}" class="nav-link {{ Request::is('abedins*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Abedins</p>
    </a>
</li>
