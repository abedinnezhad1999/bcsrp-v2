<?php

return [

    'views' => [

        'builder' => 'generator-builder::builder',

        'field-template' => 'generator-builder::field-template',

        'relation-field-template' => 'generator-builder::relation-field-template',
    ],
];

/*
Main builder view,

change 'builder' => 'generator-builder::builder' to 'builder' => 'infyom.generator-builder.builder'

Field Template view,

change 'field-template' => 'generator-builder::field-template' to 'field-template' => 'infyom.generator-builder.field-template'
*/
